from splatoon3Bot import *


bot = cqapi.create_bot(
    group_id_list=[
        # 需处理的 QQ 群信息 为空处理所有 
        # 这里填群号，多个的话都好分割       
        #1234567,7654321

    ],
)
# def testing(commandData, message: Message, command_param):
#     schedules=getModeChn(league) + '预告：\n'
#     schedules+=getSchedules(league, command_param, 2)
#     message.reply_not_code(schedules)

def re(commandData, message: Message, command_param):#涂地
    schedules = getModeChn(regular) + '预告：\n'
    schedules += getTurfSchedules(command_param, 2, True, False)
    message.reply_not_code(schedules)

def op(commandData, message: Message, command_param):#公开
    schedules=getModeChn(openEnum) + '预告：\n'
    schedules+= getSchedules(openEnum,command_param, 2,True)
    message.reply_not_code(schedules)

def cha(commandData, message: Message, command_param):#公开
    schedules=getModeChn(challenge) + '预告：\n'
    schedules+= getSchedules(challenge,command_param, 2,True)
    message.reply_not_code(schedules)
def fest(commandData, message:Message, command_param):#庆典
    schedules = getModeChn(festEnum) + '预告：\n'
    schedules += getTurfSchedules(command_param, 2, True, True)
    schedules += getFestInfo()
    message.reply_not_code(schedules)
def festInfo(commandData, message:Message, command_param):
    schedules = getModeChn(festEnum) + '信息: \n'
    schedules += getFestInfo()
    message.reply_not_code(schedules)

def helping(commandData, message: Message, command_param):
    message.reply_not_code(message.text + """全自动喷喷3播报员命令：

#涂地 : 显示最近的2场涂地比赛
#公开 : 显示最近的2场公开比赛
#挑战 : 显示最近的2场挑战比赛
#当前/现在/图 : 显示当前挑战和公开赛模式
#打工
#庆典
#庆典信息
#【模式】- 数字时间   : 例如 #公开-12将显示12~2点两个时间段的公开赛地图
""")
def requestError(commandData, message:Message, command_param):
    content = "这个指令人家认不得嘛，要不你再试试别的?\n如果你是想查询我的功能指令的话，试着输入 #？"
    message.reply_not_code(content)

def sal(commandData, message: Message, command_param):
    if command_param == -1:
        command_param = 2
    schedules = returnSalmon(datetime.now(),command_param, True)
    message.reply_not_code(schedules)

def now(commandData, message:Message, command_param):
    schedules=getModeChn(challenge) + '预告：\n'
    schedules+= getSchedules(challenge,command_param, 1,True)
    schedules += getModeChn(openEnum) + '预告：\n'
    schedules+= getSchedules(openEnum,command_param, 1,True)
    message.reply_not_code(schedules)



def runAtTen(from_id):
    sendHour = 10
    sendMinute = 40
    sendSecond = 0
    resetHour  = (sendHour + 23)%24
    global checkDirty
    nowTime = datetime.now()    
    current_hour=nowTime.hour
    if checkDirty ==False:
        if current_hour== sendHour:
            current_minute = nowTime.minute
            current_second = nowTime.second
            if current_minute == sendMinute and current_second >= sendSecond:
                checkDirty = True
                schedules="今晚的组排模式和地图为：\n"
                schedules+=getSchedules(openEnum,20, 2, True)
                checkTime = nowTime.replace(year = nowTime.year, month = nowTime.month, day = nowTime.day, hour=sendHour, minute = sendMinute, second =sendSecond, microsecond =0)
                schedules+= returnSalmon(checkTime, 1, True)
                cqapi.send_group_msg(from_id,schedules)
    if current_hour== resetHour :#注意重置时间尽量离触发远一点
        checkDirty = False


def runAtNight(from_id):
    sendHour = 18
    resetHour  = (sendHour + 23)%24
    global checkNightDirty
    nowTime = datetime.now()
    current_hour=nowTime.hour
    if checkNightDirty ==False:
        if current_hour== sendHour:
            current_minute = nowTime.minute
            current_second = nowTime.second
            if current_minute == 30 and current_second >= 0:
                checkNightDirty = True
                schedules="今晚的组排模式和地图为：\n"
                schedules+=getSchedules(openEnum, 20, 2,True)
                schedules+= returnSalmon(nowTime, 1, True)
                schedules += "祝大家排排好运！\n"
                cqapi.send_group_msg(from_id,schedules)
    if current_hour== resetHour :#注意重置时间尽量离触发远一点
        checkNightDirty = False

#每2个小时第0分跑一下 或者启动程序会跑一下，先把json抓下来
def runEveryHour(from_id):    
    global checkEveryHourDirty
    global latestJsonData
    nowTime = datetime.now()
    current_minute =int(nowTime.minute)
    checkMinute = int(0)
    resetMinute = int((checkMinute + 59)%60)
    if checkEveryHourDirty ==False:
        if current_minute == checkMinute:     
            global updateHourCount
            global updateHourMax
            updateHourCount += 1
            checkEveryHourDirty = True
            if updateHourCount >= updateHourMax or int(nowTime.hour)%2 == 0:
                updateHourCount = 0
                latestJsonData = returnJsonData('schedules.json')   
                print("update json time:" + str(nowTime))
    if current_minute == resetMinute and checkEveryHourDirty == True:#注意重置时间尽量离触发远一点
        checkEveryHourDirty = False

bot.command(now, "现在")
bot.command(now, "当前")
bot.command(now, "图")
bot.command(sal, "打工")
bot.command(helping, "帮助")
bot.command(helping, "?")
bot.command(helping, "？")
bot.command(re, "涂地")
bot.command(op, "公开")
bot.command(cha, "挑战")
bot.command(fest, "庆典")
bot.command(festInfo, "庆典信息")
bot.command(requestError, "错误")
# bot.command(testing, "测试")
cqLog()

bot.timing(runAtTen, 'runAtTen',{'timeSleep':1}) # 每1s检查一次当前时间是否是10点
bot.timing(runAtNight, 'runAtNight',{'timeSleep':1}) # 每1s检查一次当前时间是否是19点
bot.timing(runEveryHour, 'runEveryHour', {'timeSleep':1})

bot.start()