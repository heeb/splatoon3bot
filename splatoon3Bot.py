from pycqBot.cqApi import cqHttpApi, cqLog
from pycqBot import Message
from pycqBot.cqCode import image
import sys
# from read_json import regular, gachi, league
import json
from datetime import datetime, time, timedelta
import urllib3
import os
from pathlib import Path
from PIL import Image, ImageDraw, ImageFont, ImageOps
checkDirty = False
checkNightDirty = False
checkEveryHourDirty = False
reportSalmonDirty =False

reportSalmonYear = 0
reportSalmonMonth = 0
reportSalmonDay = 0
reportSalmonHour = 0
reportSalmonMinute  = 0
reportSalmonSecond = 0
updateHourCount = 0
updateHourMax = 2

regular = 1
solo = 2
league = 3
openEnum = 4
challenge = 5
festEnum = 6
xEnum = 7

imgCacheRoot ='imgCache/'
mapCachePath = 'imgCache/map/'
weaponCachePath = 'imgCache/weapon/'
clothCachePath = 'imgCache/cloth/'
utilityCachePath = 'imgCache/util/'
cqapi = cqHttpApi()
chineseMap = {}

def createCacheDir(path:str):
    if os.path.exists(path) == False:#创建目录
        if os.path.exists(imgCacheRoot) == False:
            os.mkdir(imgCacheRoot)
        os.mkdir(path)

createCacheDir(mapCachePath)
createCacheDir(weaponCachePath)
createCacheDir(clothCachePath)
createCacheDir(utilityCachePath)

def returnJsonData(jsonName:str):
    http = urllib3.PoolManager()
    r=http.request('GET', 'https://splatoon3.ink/data/' + jsonName)
    with open(jsonName,'w') as f:
        f.write(bytes.decode(r.data))
    f=open(jsonName)
    data=json.load(f)
    return data

def updateChineseJson():
    http = urllib3.PoolManager()
    r = http.request('GET','https://splatoon3.ink/data/locale/zh-CN.json')
    with open('zh-CN.json','w') as f:
        f.write(bytes.decode(r.data))
    f=open('zh-CN.json')
    data=json.load(f)
    return data

#每次程序启动加载json，没有的就从网上取，如果时间超过2小时了也重新取一次并覆盖
def getAndUpdateJsonData(jsonName:str):
    global chineseMap
    data = {}
    #open local json file if exists
    if os.path.exists(jsonName) == True:
        f=open(jsonName)    
        data = json.load(f)    
    else:
        data = returnJsonData(jsonName)#will save a new json file at local here
        
    if os.path.exists('zh-CN.json') == True:
        f=open('zh-CN.json')
        chineseMap=json.load(f)    
    else:
        chineseMap = updateChineseJson()
        
    dd = data["data"]["bankaraSchedules"]["nodes"]
    timeStart = 0
    for i in dd:           
        timeStart = datetime.strptime(i['startTime'], "%Y-%m-%dT%H:%M:%SZ")        
        timeStart += timedelta(hours = 8)
        break#only get the top time(oldest)
    nowTime = datetime.now()
    oldHour = timeStart.hour
    nowHour = nowTime.hour
    if nowHour - oldHour >= 2 or nowTime.date() > timeStart.date():
        data = returnJsonData(jsonName)
        chineseMap = updateChineseJson()
    #refresh nextsalmon time
    isBigRun = True
    isNowRunning = False
    dd = data['data']['coopGroupingSchedule']['bigRunSchedules']['nodes']
    if dd == []:
        isBigRun = False
        dd = data['data']['coopGroupingSchedule']['regularSchedules']['nodes']    
    else:
        #日程已经出现big run了，但是时间实际上还没到，应该继续显示普通打工的日程
        regulardata = data['data']['coopGroupingSchedule']['regularSchedules']['nodes']
        regularCount = 0
        for i in regulardata:
            regularTime = datetime.strptime(i['startTime'], "%Y-%m-%dT%H:%M:%SZ")
            bigrunTime = datetime.strptime(dd[0]['startTime'], "%Y-%m-%dT%H:%M:%SZ")
            if bigrunTime > regularTime:
                regularCount +=1
        
        bigRunStartTime = datetime.strptime(dd[0]['startTime'], "%Y-%m-%dT%H:%M:%SZ") + timedelta(hours = 8)        
        if regularCount > 1 :
            #regularCount > 1说明bigrun的时间还间隔了至少一次普通打工，下一次的打工依然是普通打工            
            dd = data['data']['coopGroupingSchedule']['regularSchedules']['nodes']
            isBigRun = False
            
        if regularCount == 0 and datetime.now() > bigRunStartTime:
            #第二个判断条件是通过时间判断现在处于bigrun时期了，需要显示下一次打工
            dd = data['data']['coopGroupingSchedule']['regularSchedules']['nodes']
            isBigRun = False
            isNowRunning = True

    index = 0
    for i in dd:
        if isBigRun == False:
            if index < 1 and isNowRunning== False:
                index += 1
                continue        
        startTime = datetime.strptime(i['startTime'], "%Y-%m-%dT%H:%M:%SZ")
        startTime += timedelta(hours = 8)
        global reportSalmonHour 
        global reportSalmonMinute 
        global reportSalmonSecond 
        global reportSalmonDay
        global reportSalmonYear
        global reportSalmonMonth
        reportSalmonYear = (int)(startTime.year)
        reportSalmonMonth = (int)(startTime.month)
        reportSalmonDay = (int)(startTime.day)
        reportSalmonHour = (int)(startTime.hour)
        reportSalmonMinute = (int)(startTime.minute)
        reportSalmonSecond =(int)(startTime.second)
        break
            
    return data



def getImageFromNet(url:str, fileName:str, cachePath:str):
    http = urllib3.PoolManager()
    r=http.request('GET', url)
    with open(cachePath + fileName,'wb') as f:
        f.write(r.data)
        f.close()

def getImageFromLocalCache(url:str, cachePath:str):
    fileName = url[url.rfind('/')+1:]
    fullPath = cachePath + fileName
    if os.path.exists(fullPath) == True:            
        return fullPath
    else:
        getImageFromNet(url, fileName, cachePath)
        return getImageFromLocalCache(url, cachePath)

def getPathAsUrl(path:str, noUrl:bool = False):
    ret = Path(path)
    if noUrl == False:
        return ret.absolute().as_uri()#返回的是一个url形式的本地地址，这样image()函数可以直接调用
    else:
        ppp = ret.absolute().as_uri()#返回的是一个url形式的本地地址，这样image()函数可以直接调用        
        return ppp[7:]#去掉前面的file:///
    
def getSalmonKingIconPath(kingname):
    if kingname == "Cohozuna":
        return utilityCachePath + 'king-cohozuna.278e10f7.png'
    elif kingname == "Horrorboros":
        return utilityCachePath + 'king-horrorboros.9dbd4964.png'
    
def getMapChineseName(englishName:str, type:str = 'stages'):
    # if englishName == "Gone Fission Hydroplant":
    #     return '麦年海洋发电所'
    # elif englishName == "Spawning Grounds":
    #     return "鲑坝"
    # elif englishName == "Sockeye Station":
    #     return "新卷堡"
    # elif englishName == "Hagglefish Market":
    #     return '烟管鱼市场'
    # elif englishName== 'MakoMart':
    #     return '座头购物中心'
    # elif englishName == "Hammerhead Bridge":
    #     return '真鲭跨海大桥'
    # elif englishName == 'Museum d\'Alfonsino':
    #     return '金眼鲷美术馆'
    # elif englishName == 'Mahi-Mahi Resort':
    #     return '鬼头刀SPA度假区'
    # elif englishName == 'Inkblot Art Academy':
    #     return '海女美术大学'
    # elif englishName == 'Sturgeon Shipyard':
    #     return '鲟鱼造船厂'
    # elif englishName == 'Wahoo World':
    #     return '醋饭海洋世界'
    # elif englishName == 'Eeltail Alley':
    #     return '鳗鲶区'
    # elif englishName == 'Undertow Spillway':
    #     return '竹蛏疏洪道'
    # elif englishName == 'Mincemeat Metalworks':
    #     return '鱼肉碎金属'
    # elif englishName == 'Scorch Gorge':
    #     return '温泉花大峡谷'
    # elif englishName == 'Brinewater Springs':
    #     return '臭鱼干温泉'
    # elif englishName == 'Flounder Heights':
    #     return '比目鱼住宅区'
    # elif englishName == "Marooner's Bay":
    #     return '漂浮落难船'
    # elif englishName == 'Manta Maria':
    #     return '鬼蝠鲼玛丽亚号'
    # elif englishName == "Um'ami Ruins":
    #     return '鱼露遗迹'
    # elif englishName == "Humpback Pump Track":
    #     return '昆布赛道'
    # elif englishName == "Barnacle & Dime":
    #     return '塔拉波特购物公园'
    # elif englishName == "Jammin' Salmon Junction":
    #     return '生筋子系统交流道遗址'
    # elif englishName == "Crableg Capital":
    #     return '高脚经济特区'
    # elif englishName == "Shipshape Cargo Co.":
    #     return '大比目鱼海运中心'
    # else:
    #     return englishName
    global chineseMap
    if chineseMap == {}:
        chineseMap = updateChineseJson()
        name = chineseMap[type][englishName]['name']
        return name
    else:
        return '默认'

def getModeChn(mode):
    modeChn = ' '
    if mode == regular :
        modeChn = '『涂地』'
    elif mode == solo:
        modeChn = '『单排』'
    elif mode == league:
        modeChn = '『组排』'
    elif mode == challenge:
        modeChn = '『挑战』'
    elif mode == openEnum:
        modeChn = '『公开』'
    elif mode == festEnum:
        modeChn = '『庆典』'
    elif mode == xEnum:
        modeChn = '『X比赛』'
    return modeChn

def getTurfSchedules(time, itemCount: int= -1, ismap:bool = False, isFest :bool = False):
    global chineseMap
    data = getAndUpdateJsonData('schedules.json')
    imageListA = []
    imageListB = [] 
    imageNameA = []   
    imageNameB = []
    count = 0    
    dataSet = {}
    dd = {}
    if isFest == True:
        dd = data["data"]["festSchedules"]["nodes"]
    else:
        dd = data["data"]["regularSchedules"]['nodes']
    for i in dd:           
        timeStart = datetime.strptime(i['startTime'], "%Y-%m-%dT%H:%M:%SZ")        
        timeStart += timedelta(hours = 8)     
        bank = {}   
        if isFest == True:
            bank = i['festMatchSetting']
        else:
            bank = i['regularMatchSetting']
        if None == bank :
            continue
            # schedules= "[庆典期间涂地比赛当前不可用]\n"#庆典时会来这里
            # schedules += getTurfSchedules(time, itemCount, ismap, True)
            # return schedules            
        rule = bank["vsRule"]["name"]                   
        j0= bank['vsStages'][0]                    
        dataSet[str(timeStart)] = rule
        imageListA.append(j0['image']['url'])      
        # imageNameA.append(j0['name'])       
        imageNameA.append(j0['id'])  
        j1 = bank['vsStages'][1]
        imageListB.append(j1['image']['url'])       
        # imageNameB.append(j1['name'])
        imageNameB.append(j1['id'])
    schedules=''    
    schedules+='-------------------\n'
    index = 0
    beginShowDirty = False
    for key,value in dataSet.items():
        if str(value)=='Splat Zones':
            value='区域 ▼'
        elif str(value)=='Tower Control':
            value='塔 ▼'
        elif str(value)=='Clam Blitz':
            value='蛤蜊 ▼'
        elif str(value)=='Rainmaker':
            value='鱼 ▼'
        elif str(value)=='Turf War':
            value='涂地 ▼'
        if time == -1 :
            time = datetime.now().hour
        if time != -1 and time % 2 != 0 :
            time -= 1
        if(int(str(key[11:13])) == time):
            beginShowDirty = True
        if(itemCount != -1 and count >= itemCount):#itemCount == -1 则显示全部
            break
        if beginShowDirty == True:
            count += 1
            realNowTime = str(key[11:13])
            key=str(key[5:7]) + '月' + str(key[8:10])+'日'+str(key[11:13])+':00'
            schedules+=str(key)+" : "+str(value)+"\n"
            if ismap == True:
                back = Image.open(getPathAsUrl(utilityCachePath + 'mapback.png', True))
                back = back.resize((back.width, back.height + 30),  Image.BILINEAR)
                image1 = Image.open(getPathAsUrl(getImageFromLocalCache(imageListA[index], mapCachePath), True))
                image2 = Image.open(getPathAsUrl(getImageFromLocalCache(imageListB[index], mapCachePath), True))
                back.paste(image1,(10,80))
                back.paste(image2,(420,80))

                font = ImageFont.truetype('AaHouDiHei-2.ttf',36)
                draw = ImageDraw.Draw(back)

                text = getMapChineseName(imageNameA[index])                
                w = len(text) * 36
                draw.text(((back.width / 2- w)/2, 295), text, font=font, fill='#FFFFFF',spacing = 0, align = 'left')
                text = getMapChineseName(imageNameB[index])
                w = len(text)* 36
                draw.text(((back.width * 3 / 2 - w)/2, 295), text, font=font, fill='#FFFFFF',spacing = 0, align = 'left')
                regularImg = Image.open(getPathAsUrl(utilityCachePath + 'regular.png', True)).convert('RGBA')
                regularImg = regularImg.resize((int(regularImg.width/8), int(regularImg.height/8)), Image.BILINEAR)
                r,g,b,a = regularImg.split()
                back.paste(regularImg, ((int)((back.width-regularImg.width) / 2), (int)(image1.height / 2) + 80 - (int)(regularImg.height / 2)), mask = a)
                nowTime = datetime.now()
                nowTime =datetime(nowTime.year, nowTime.month, nowTime.day, (int(realNowTime)))
                endTime = nowTime +  timedelta(hours = 2)
                extraZero = ''
                if endTime.hour < 10 :
                    extraZero = '0'
                timeText =  realNowTime + ':00~' + extraZero + str(endTime.hour) + ':00'
                tw, th = draw.textsize(timeText,font)
                draw.text(((int)((back.width - tw)/2) , 17),timeText,  font=font, fill='#FFFFFF',spacing = 0, align = 'right')
                # back.show()#debug
                finalName =imgCacheRoot +  str(index)+ value + 'turf.png'
                back.save(finalName)
                schedules +=  image("head.png", getPathAsUrl(finalName))
            schedules+='-------------------\n'  
        index+=1      
    return schedules

def getSchedules(mode, time ,itemCount: int = -1, ismap: bool = False):
    global chineseMap
    scheduleType = ' '
    matchSetting = ' '
    modeStr = ' '
    matchLogo = 'bankara'
    if mode == openEnum:
        modeStr = 'OPEN'    
        matchSetting = 'bankaraMatchSettings'   
        scheduleType = 'bankaraSchedules'
    elif mode == challenge:
        modeStr = 'CHALLENGE'    
        matchSetting = 'bankaraMatchSettings'
        scheduleType = 'bankaraSchedules'
    elif mode == xEnum:
        matchSetting = 'xMatchSetting'
        scheduleType = 'xSchedules'
        matchLogo = 'xmatch'
    
    data = getAndUpdateJsonData('schedules.json')
    imageListA = []
    imageListB = []
    imageNameA = []
    imageNameB = []    
    count = 0    
    dataSet = {}
    dd = data["data"][scheduleType]["nodes"]
    preCount = 0
    startDirty = False
    if time == -1 :
        time = datetime.now().hour
    if time != -1 and time % 2 != 0 :
        time -= 1
    for i in dd:           
        timeStart = datetime.strptime(i['startTime'], "%Y-%m-%dT%H:%M:%SZ")
        timeStart += timedelta(hours = 8)        
        if timeStart.hour != time and startDirty == False:
            continue
        else:
            startDirty = True
        preCount+=1
        if preCount > itemCount:
            break
        bank = i[matchSetting]
        if None == bank :            
            schedules= "[庆典期间蛮頽比赛不可用]\n"#庆典时会来这里
            schedules += getTurfSchedules(time, itemCount, ismap, True)
            return schedules
        if mode != xEnum:
            for k in bank:          
                rule = k["vsRule"]["name"]
                Mode = k['bankaraMode']            
                if Mode == modeStr or Mode == '':
                    j0= k['vsStages'][0]                    
                    dataSet[str(timeStart)] = rule
                    imageListA.append(j0['image']['url'])    
                    # imageNameA.append(j0['name'])         
                    imageNameA.append(j0['id'])  
                    j1 = k['vsStages'][1]
                    imageListB.append(j1['image']['url'])       
                    # imageNameB.append(j1['name'])      
                    imageNameB.append(j1['id'])   
        else:#x match
            rule = bank["vsRule"]["name"]
            j0= bank['vsStages'][0]                    
            dataSet[str(timeStart)] = rule
            imageListA.append(j0['image']['url'])    
            # imageNameA.append(j0['name'])         
            imageNameA.append(j0['id'])         
            j1 = bank['vsStages'][1]
            imageListB.append(j1['image']['url'])       
            # imageNameB.append(j1['name'])    
            imageNameB.append(j1['id'])    
    schedules=''        
    index = 0
    beginShowDirty = False
    for key,value in dataSet.items():
        modeIcon= ''
        if str(value)=='Splat Zones':
            value='真格区域'
            modeIcon = utilityCachePath + 'area.png'
        elif str(value)=='Tower Control':
            value='真格塔楼'
            modeIcon = utilityCachePath + 'yagura.png'
        elif str(value)=='Clam Blitz':
            value='真格蛤蜊'
            modeIcon = utilityCachePath + 'asari.png'
        elif str(value)=='Rainmaker':
            value='真格虎鱼'
            modeIcon = utilityCachePath + 'hoko.png'
        if time == -1 :
            time = datetime.now().hour
        if time != -1 and time % 2 != 0 :
            time -= 1
        if(int(str(key[11:13])) == time):
            beginShowDirty = True
        if(itemCount != -1 and count >= itemCount):#itemCount == -1 则显示全部
            break
        if beginShowDirty == True:
            realNowTime = str(key[11:13])
            key=str(key[5:7]) + '月' + str(key[8:10])+'日'+str(key[11:13])+':00'
            schedules+=str(key)+" : "+str(value)+" ▼\n"
            if ismap == True:                
                back = Image.open(getPathAsUrl(utilityCachePath + 'mapback.png', True))
                back = back.resize((back.width, back.height + 30),  Image.BILINEAR)
                image1 = Image.open(getPathAsUrl(getImageFromLocalCache(imageListA[index], mapCachePath), True))
                image2 = Image.open(getPathAsUrl(getImageFromLocalCache(imageListB[index], mapCachePath), True))
                back.paste(image1,(10,80))
                back.paste(image2,(420,80))

                font = ImageFont.truetype('AaHouDiHei-2.ttf',36)
                draw = ImageDraw.Draw(back)

                text = getMapChineseName(imageNameA[index])                
                w = len(text) * 36
                draw.text(((back.width / 2- w)/2, 290), text, font=font, fill='#FFFFFF',spacing = 0, align = 'left')
                text = getMapChineseName(imageNameB[index])
                w = len(text)* 36
                draw.text(((back.width * 3 / 2 - w)/2, 290), text, font=font, fill='#FFFFFF',spacing = 0, align = 'left')
                modeIconImg = Image.open(getPathAsUrl(modeIcon, True)).convert('RGBA')
                r,g,b,a = modeIconImg.split()
                back.paste(modeIconImg, (15, 17), mask = a)
                bankaraImg = Image.open(getPathAsUrl(utilityCachePath + matchLogo + '.png', True)).convert('RGBA')
                bankaraImg = bankaraImg.resize((int(bankaraImg.width/8), int(bankaraImg.height/8)), Image.BILINEAR)
                r,g,b,a = bankaraImg.split()
                back.paste(bankaraImg, ((int)((back.width-bankaraImg.width) / 2), (int)(image1.height / 2) + 80 - (int)(bankaraImg.height / 2)), mask = a)
                draw.text( (20 + modeIconImg.width, 17 ), value, font=font, fill='#FFFFFF',spacing = 0, align = 'left')
                                
                nowTime = datetime.now()
                nowTime =datetime(nowTime.year, nowTime.month, nowTime.day, (int(realNowTime)))
                endTime = nowTime +  timedelta(hours = 2)
                extraZero = ''
                if endTime.hour < 10 :
                    extraZero = '0'
                timeText =  realNowTime + ':00~' +extraZero + str(endTime.hour) + ':00'
                tw, th = draw.textsize(timeText,font)
                draw.text(((int)(back.width) - tw - 30 , 17),timeText,  font=font, fill='#FFFFFF',spacing = 0, align = 'right')
                # back.show()#debug
                finalName =imgCacheRoot + value + 'head.png'
                back.save(finalName)                
                schedules +=  image("head.png", getPathAsUrl(finalName))
            else:
                schedules += getMapChineseName(imageNameA[index])  
                schedules += '\n'+getMapChineseName(imageNameB[index]) + '\n'  
            count += 1
            schedules+='-------------------\n'  
        index+=1      
        
    return schedules

def returnSalmon(nowTime, count : int = 2, ismap: bool = False):   
    global chineseMap 
    data = getAndUpdateJsonData('schedules.json')
    schedules=''
    bigRunStr = 'BigRun大型跑'
    rr = data['data']['coopGroupingSchedule']['bigRunSchedules']['nodes']

    if rr == []:
        rr = data['data']['coopGroupingSchedule']['regularSchedules']['nodes']
        bigRunStr = ''
    else:#日程已经出现big run了，但是时间实际上还没到，应该继续显示普通打工的日程
        regularTime = datetime.strptime(data['data']['coopGroupingSchedule']['regularSchedules']['nodes'][0]['startTime'], "%Y-%m-%dT%H:%M:%SZ")
        bigrunTime = datetime.strptime(rr[0]['startTime'], "%Y-%m-%dT%H:%M:%SZ")
        if bigrunTime > regularTime:
            rr = data['data']['coopGroupingSchedule']['regularSchedules']['nodes']
            bigRunStr = ''

    def salmon(dd, info, count):
        schedules=''
        index = 0
        for i in dd:
            if index >= count:
                break
            
            startTime = datetime.strptime(i['startTime'], "%Y-%m-%dT%H:%M:%SZ")  
            endTime = datetime.strptime(i['endTime'], "%Y-%m-%dT%H:%M:%SZ")      
            startTime += timedelta(hours = 8)
            endTime += timedelta(hours = 8)        
            if startTime <= nowTime < endTime :
                schedules += "『" + info + "打工开放中！』\n"
                schedules += "距离『收工』还剩："
                restTime = endTime - nowTime
                if restTime.days > 0:
                    schedules += str(restTime.days) + '天'
                schedules += str(restTime.seconds//3600) + '时' + str((restTime.seconds//60)%60) + '分' + str(restTime.seconds%60) + '秒'+ '\n'
            elif nowTime < startTime:
                schedules += "『"+ info + "尚未开放 Zzz..』\n"
                schedules += "距离『开工』还剩："
                restTime = startTime - nowTime
                if restTime.days > 0:
                    schedules += str(restTime.days) + '天'
                schedules += str(restTime.seconds//3600) + '时' + str((restTime.seconds//60)%60) + '分' + str(restTime.seconds%60) + '秒'+ '\n'
            else:
                # schedules += "『已过期...』\n"
                schedules = ''
                continue
            index+=1
            schedules += "开始时间：『"
            schedules += (str(startTime))
            schedules += "』\n结束时间：『"
            schedules += (str(endTime)) + "』 ▼"
            schedules += '\n'
            print(i['setting']['coopStage']['image']['url'])
            if ismap == True:
                mapImg = getImageFromLocalCache(str(i['setting']['coopStage']['image']['url']), mapCachePath) 
                back = Image.open(getPathAsUrl(utilityCachePath + 'salmonBack.png', True))
                back = back.resize(((int)(back.width *0.6),(int)(back.height * 0.6)), Image.BILINEAR)
                backWidth = (int)(back.width *0.6)
                quaterWidth = (int)(backWidth/4)
                halfQuaterWidth = (int)(quaterWidth/2)
                img= Image.open(getPathAsUrl(mapImg,True))
                back.paste(img, (8, 150))
                gap = 0
                weapon = 0
                for k in i['setting']['weapons']:
                    weaponImg = getImageFromLocalCache(str(k['image']['url']), weaponCachePath)
                    modeIcon = Image.open(getPathAsUrl(weaponImg, True)).convert('RGBA')
                    if modeIcon.height > 256 or modeIcon.width > 256 or modeIcon.height < 256 or modeIcon.width < 256 :
                        modeIcon = modeIcon.resize((256,256), Image.BILINEAR)
                    small = modeIcon.resize(((int)(modeIcon.size[0]*0.5), (int)(modeIcon.size[1]*0.5)), Image.BILINEAR)
                    r,g,b,a = small.split()
                    back.paste(small, (38 + (weapon)*small.width + gap, 639),mask =a)
                    

                    # 将图像转换为灰度图像
                    gray_img = ImageOps.grayscale(small)
                    # 将图像转换为反色图像
                    inverted_img = ImageOps.invert(gray_img)
                    # 将图像转换为二值图像
                    threshold = 255
                    binary_img = inverted_img.point(lambda x: 0 if x < threshold else 255, '1')
                    #先画阴影                
                    back.paste(binary_img, (43 + (weapon)*small.width + gap, 644), mask = a)
                    #再画武器本身
                    back.paste(small, (38 + (weapon)*small.width + gap, 639),mask =a)

                    gap += halfQuaterWidth                    
                    weapon += 1
                
                font = ImageFont.truetype('AaHouDiHei-2.ttf',72)
                draw = ImageDraw.Draw(back)

                # text = getMapChineseName(i['setting']['coopStage']['name'])
                text = getMapChineseName(i['setting']['coopStage']['id'])
                w = len(text) * 72
                draw.text(((back.width - w)/2, 57), text, font=font, fill='#FFFFFF',spacing = 0, align = 'center')
                #boss图标
                if '__splatoon3ink_king_salmonid_guess' in i:
                    bossIconNode = i['__splatoon3ink_king_salmonid_guess']
                    bossIconPath = getSalmonKingIconPath(str(bossIconNode))
                    bossIcon = Image.open(getPathAsUrl(bossIconPath, True)).convert('RGBA')
                    halfSize = (int)(bossIcon.size[0])
                    r,g,b,a = bossIcon.split()
                    ww, hh = (font.getsize(text) )
                    # back.paste(bossIcon, ((int)((back.width-ww)/2) - halfSize, 0), mask = a)#在地图文字的开头，但文字可能很长，把icon顶到画面外面
                    back.paste(bossIcon, (8, 150),mask = a)#挂在地图图片的左上角
                    draw = ImageDraw.Draw(back)
                
                # back.show()#debug

                finalName = imgCacheRoot + str(i['setting']['__typename']) + str(index) + "salmon.png"
                back.save(finalName)               
                schedules +=  image("head.png", getPathAsUrl(finalName))
                schedules += '\n'        

            schedules+='-------------------\n'    
        return schedules
    #正常的打工或者大型跑
    schedules += salmon(rr, bigRunStr, count)
    #团队打工出现的时候，正常打工是同时存在的，非互斥关系
    jj = data['data']['coopGroupingSchedule']['teamContestSchedules']['nodes']
    if jj != []:
        schedules += salmon(jj, "团队打工竞赛", 1)
    return schedules

#特殊模式
def getEventSchedules():
    data = getAndUpdateJsonData('schedules.json')
    rr = data['data']['eventSchedules']['nodes']
    for i in rr:
        matchSettingNode = i['leagueMatchSetting']
        timePeriodNode = i['timePeriods']


def getFestInfo():
    data = getAndUpdateJsonData('schedules.json')
    schedules=''
    dd = data['data']['currentFest']
    title = dd['title']
    state = dd['state']#'FIRST_HALF'
    if state == 'FIRST_HALF':
        state
    startTime = datetime.strptime(dd['startTime'], "%Y-%m-%dT%H:%M:%SZ")  
    endTime = datetime.strptime(dd['endTime'], "%Y-%m-%dT%H:%M:%SZ") 
    midTime = datetime.strptime(dd['midtermTime'],"%Y-%m-%dT%H:%M:%SZ")      
    startTime += timedelta(hours = 8)
    endTime += timedelta(hours = 8)
    midTime += timedelta(hours = 8)
    triColorStageImageUrl = dd['tricolorStage']['image']['url']
    schedules += '庆典标题：'+ title
    schedules += '\n起始时间：' + str(startTime)
    schedules += '\n中间时间：' + str(midTime)
    schedules += '\n结束时间：' + str(endTime)
    schedules += '\n上下半场：' + state
    schedules += '\n三色地图：' + image('weapon' + '.jpg', triColorStageImageUrl)
    schedules += '\n'
    return schedules
# 设置指令为 echo
# bot.command(re, "涂地", {
#     # echo 帮助
#     "help": [
#         "#echo - 输出文本"
#     ]
# })


